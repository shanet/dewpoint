#include "dewpoint.h"

DHT dht(DHTPIN, DHTTYPE);

void dhtSetup() {
  dht.begin();
}

bool dhtRead(float *dewPoint, float *humidity, float *temperature) {
  *humidity = dht.readHumidity();
  *temperature = dht.readTemperature();
  *dewPoint = calculateDewPoint(*humidity, *temperature);

  return true;
}

float calculateDewPoint(float humidity, float temperature) {
  return (257.14 * helper(humidity, temperature)) / (18.678 - helper(humidity, temperature));
}

float helper(float humidity, float temperature) {
  return log(humidity / 100) + ((18.678 * temperature) / (257.14 + temperature));
}
