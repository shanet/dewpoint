#ifndef DEWPOINT_H
#define DEWPOINT_H

#include <DHT.h>
#include "constants.h"

extern DHT dht;

void dhtSetup();
bool dhtRead(float *dewPoint, float *humidity, float *temperature);
float calculateDewPoint(float humidity, float temperature);
float helper(float humidity, float temperature);

#endif
