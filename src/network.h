#ifndef NETWORK_H
#define NETWORK_H

#include <WiFi101.h>

#include "constants.h"
#include "dewpoint.h"
#include "secrets.h"

void wifiSetup();
bool connectToNetwork(IPAddress *ip, IPAddress *dns, IPAddress *gateway, IPAddress *subnet);
void processMessage(Client &client);
void abortClient(Client &client);
void flashLed();

int networkStatus = WL_IDLE_STATUS;

#endif
