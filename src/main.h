#ifndef MAIN_H
#define MAIN_H

#include <SPI.h>
#include <WiFi101.h>

#include "constants.h"
#include "dewpoint.h"
#include "secrets.h"

// IMPORTANT: The network settings MUST BE CHANGED to something unique for each server
IPAddress ip(10, 10, 12, 40);
IPAddress dns(10, 10, 12, 1);
IPAddress gateway(10, 10, 12, 1);
IPAddress subnet(255, 255, 255, 0);

WiFiServer server(PORT);

extern void processMessage(Client &client);
extern void wifiSetup();
extern bool connectToNetwork(IPAddress *ip, IPAddress *dns, IPAddress *gateway, IPAddress *subnet);
extern void flashLed();

#endif
