#define PORT 2424
#define OK   "OK"
#define ERR  "ERR"

#define SUCCESS 0
#define FAILURE -1

#define DHTPIN 10
#define DHTTYPE DHT22

#define LED_PIN 13
#define LED_FLASH_DELAY 500 // ms
